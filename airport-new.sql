-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2014 at 01:32 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

CREATE DATABASE IF NOT EXISTS `airport-new` /*!40100 DEFAULT CHARACTER SET latin1 */
USE `airport-new`;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `airport-new`
--

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

CREATE TABLE IF NOT EXISTS `flights` (
  `flight_number` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `departure` datetime NOT NULL,
  `arrival` datetime NOT NULL,
  UNIQUE KEY `flight_number` (`flight_number`),
  KEY `flight_number_2` (`flight_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`flight_number`, `destination`, `departure`, `arrival`) VALUES
('10791', 'London', '2014-02-20 05:05:39', '2014-02-21 04:55:25'),
('1452832', 'Madrid', '2014-02-26 06:16:19', '2014-02-27 02:48:48'),
('15463', 'Zurich', '2014-02-21 04:37:00', '2014-02-28 07:17:00'),
('27590', 'Paris', '2014-02-05 04:22:39', '2014-02-07 11:06:09');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `ticket_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` int(100) NOT NULL,
  `seat_number` int(100) NOT NULL,
  UNIQUE KEY `ticket_id` (`ticket_id`),
  UNIQUE KEY `ticket_id_2` (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
